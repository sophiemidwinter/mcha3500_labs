#include <stdint.h>
#include <stdlib.h>
#include <math.h> 
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h" 
#include "uart.h"
#include "data_logging.h"
#include "pendulum.h"
#include "IMU.h"

/* Variable declarations */
uint16_t logCount = 0;
float voltage;
float ang_velocity;
double imu_angle;
static osTimerId_t dataLoggingTimerId;
static osTimerAttr_t dataLoggingTimerAttr = 
{
	.name = "dataLoggingTimer"
};
static void (*log_function)(void);

/* Function declarations */
static void log_pendulum(void);
static void log_pointer(void *argument);
static void log_imu(void);


/* Function definitions */

void logging_init(void)
{
	/* Initialise timer for use with pendulum data logging */
	dataLoggingTimerId = osTimerNew(log_pointer, osTimerPeriodic, NULL, &dataLoggingTimerAttr);
}

static void log_pointer(void *argument)
{
	UNUSED(argument);
	
	/* Call function pointed to by log_function */
	(*log_function)();
}

void pend_logging_start(void)
{
	log_function = &log_pendulum;
	logCount = 0;
	osTimerStart(dataLoggingTimerId, 5);
}

void logging_stop(void)
{
	osTimerStop(dataLoggingTimerId);
}

static void log_pendulum(void)
{
	/* Supress compiler warnings for unused arguments
	UNUSED(argument);*/

	/* Read the potentiometer voltage */
	voltage = pendulum_read_voltage();

	/* Print the sample time and potentiometer voltage to the serial terminal in the format [time],[
	   voltage] */
	printf("%f,%f\n", logCount*0.005, voltage);

	/* Increment log count */
	logCount ++;

	/* Stop logging once 2 seconds is reached (Complete this once you have created the stop function
	   in the next step) */
	if(logCount*0.005 >= 2)
	{
		logging_stop();
	}
	
}

void imu_logging_start(void)
{
	log_function = &log_imu;
	logCount = 0;
	osTimerStart(dataLoggingTimerId, 5);
}

static void log_imu(void)
{
	/* TODO: Read IMU */
	IMU_read();

	/* TODO: Get the imu angle from accelerometer readings */
	imu_angle = get_acc_angle();
	
	/* TODO: Get the imu X gyro reading */
	ang_velocity = get_gyoX();

	/* TODO: Read the potentiometer voltage */
	voltage = pendulum_read_voltage();

	/* TODO: Print the time, accelerometer angle, gyro angular velocity and pot voltage values to the
		serial terminal in the format %f,%f,%f,%f\n */
	printf("%f,%f,%f,%f\n", logCount*0.005, imu_angle, ang_velocity, voltage);

	/* TODO: Increment log count */
	logCount ++;

	/* TODO: Stop logging once 5 seconds is reached */
	if(logCount*0.005 >= 5)
	{
		logging_stop();
	}
}

