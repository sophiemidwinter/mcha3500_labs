#include <stddef.h>
#include "stm32f4xx_hal.h"
#include "arm_math.h" /* Include STM32 DSP matrix libraries */
#include "qpas_sub_noblas.h"
#include "controller.h"

// #define CTRL_N_INPUT 1
// #define CTRL_N_STATE 5

// Variables for QP solver
int numits,numadd,numdrop;

// Define actuator limits
#define u_min -1.0
#define u_max 1.0
#define delta_u_min -0.1
#define delta_u_max 0.1

// /* Define control matrix values */
// static float ctrl_mK_f32[CTRL_N_INPUT*CTRL_N_STATE] =
// {
	// /* negative K, 1x5 */
	// 57.4827, 162.0246, 49.4904, 26.5967, 29.8511
// };
// static float ctrl_x_f32[CTRL_N_STATE] =
// {
	// /* estimate of state, 5x1 */
	// 0.0,
	// 0.0,
	// 0.0,
	// 0.0,
	// 0.0,
// };
// static float ctrl_u_f32[CTRL_N_INPUT] =
// {
	// /* control action, 1x1 */
	// 0.0,
// };
// static float ctrl_Az_f32[CTRL_N_STATE] =
// {
	// /* State transition matrix */
	// 0.0050, 0, 0, 0, 1,
// };
// static float ctrl_z_f32[CTRL_N_INPUT] =
// {
	// /* Integrator state */
	// 0.0,
// };

// /* Define control matrix variables */
// // rows, columns, data array
// arm_matrix_instance_f32 ctrl_mK = {CTRL_N_INPUT, CTRL_N_STATE, (float32_t *)ctrl_mK_f32};
// arm_matrix_instance_f32 ctrl_x = {CTRL_N_STATE, 1, (float32_t *)ctrl_x_f32};
// arm_matrix_instance_f32 ctrl_u = {CTRL_N_INPUT, 1, (float32_t *)ctrl_u_f32};
// arm_matrix_instance_f32 ctrl_Az = {1, CTRL_N_STATE, (float32_t *)ctrl_Az_f32};
// arm_matrix_instance_f32 ctrl_z = {1, 1, (float32_t *)ctrl_z_f32};

// /* Control functions */
// void ctrl_init(void)
// {
	// arm_mat_init_f32(&ctrl_mK, CTRL_N_INPUT, CTRL_N_STATE, (float32_t *)ctrl_mK_f32);
	// arm_mat_init_f32(&ctrl_x, CTRL_N_STATE, 1, (float32_t *)ctrl_x_f32);
	// arm_mat_init_f32(&ctrl_u, CTRL_N_INPUT, 1, (float32_t *)ctrl_u_f32);
	// arm_mat_init_f32(&ctrl_Az, 1, CTRL_N_STATE, (float32_t *)ctrl_Az_f32);
	// arm_mat_init_f32(&ctrl_z, 1, 1, (float32_t *)ctrl_z_f32);
// }


/* Define control arrays */
// Hessian
static float32_t ctrl_H_f32[CTRL_N_HORIZON*CTRL_N_HORIZON] =
{
	0.0028,    0.0007,    0.0007,    0.0007,    0.0006,    0.0006,    0.0005,    0.0005,    0.0005,    0.0004,
    0.0007,    0.0027,    0.0007,    0.0006,    0.0006,    0.0005,    0.0005,    0.0005,    0.0004,    0.0004,
    0.0007,    0.0007,    0.0026,    0.0006,    0.0006,    0.0005,    0.0005,    0.0005,    0.0004,    0.0004,
    0.0007,    0.0006,    0.0006,    0.0026,    0.0005,    0.0005,    0.0005,    0.0004,    0.0004,    0.0004,
    0.0006,    0.0006,    0.0006,    0.0005,    0.0025,    0.0005,    0.0004,    0.0004,    0.0004,    0.0004,
    0.0006,    0.0005,    0.0005,    0.0005,    0.0005,    0.0024,    0.0004,    0.0004,    0.0004,    0.0003,
    0.0005,    0.0005,    0.0005,    0.0005,    0.0004,    0.0004,    0.0024,    0.0004,    0.0003,    0.0003,
    0.0005,    0.0005,    0.0005,    0.0004,    0.0004,    0.0004,    0.0004,    0.0023,    0.0003,    0.0003,
    0.0005,    0.0004,    0.0004,    0.0004,    0.0004,    0.0004,    0.0003,    0.0003,    0.0023,    0.0003,
    0.0004,    0.0004,    0.0004,    0.0004,    0.0004,    0.0003,    0.0003,    0.0003,    0.0003,    0.0023,
};
// f bar
static float32_t ctrl_fBar_f32[CTRL_N_HORIZON*CTRL_N_STATE] =
{
   -0.0514,   -0.5742,   -0.1537,   -0.0890,
   -0.0473,   -0.5409,   -0.1446,   -0.0838,
   -0.0435,   -0.5090,   -0.1360,   -0.0787,
   -0.0398,   -0.4785,   -0.1277,   -0.0740,
   -0.0363,   -0.4494,   -0.1197,   -0.0694,
   -0.0330,   -0.4215,   -0.1121,   -0.0650,
   -0.0298,   -0.3947,   -0.1048,   -0.0608,
   -0.0268,   -0.3690,   -0.0978,   -0.0568,
   -0.0240,   -0.3442,   -0.0911,   -0.0530,
   -0.0213,   -0.3203,   -0.0846,   -0.0492,
};
// f
static float32_t ctrl_f_f32[CTRL_N_HORIZON] =
{
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
};
// State vector
static float32_t ctrl_xHat_f32[CTRL_N_STATE] =
{
	0.0,
	0.0,
	0.0,
	0.0,
};
// Control
static float32_t ctrl_u_f32[CTRL_N_INPUT] =
{
	0.0,
};
// U star
static float32_t ctrl_Ustar_f32[CTRL_N_HORIZON*CTRL_N_INPUT] =
{
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
};
// Constraints
static float ctrl_A_f32[CTRL_N_INEQ_CONST*CTRL_N_HORIZON] =
{
     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,
     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,
     0.0,     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,
     0.0,     0.0,     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,     0.0,     0.0,     0.0,     0.0,
     0.0,     0.0,     0.0,     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,     0.0,     0.0,     0.0,
     0.0,     0.0,     0.0,     0.0,     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,     0.0,     0.0,
     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,     0.0,
     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,
     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,
     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,
};
static float ctrl_b_f32[CTRL_N_INEQ_CONST] =
{
	delta_u_max,
	delta_u_max,
	delta_u_max,
	delta_u_max,
	delta_u_max,
	delta_u_max,
	delta_u_max,
	delta_u_max,
	delta_u_max,
	delta_u_max,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
};
static float ctrl_xl_f32[CTRL_N_LB_CONST] =
{
	u_min,
	u_min,
	u_min,
	u_min,
	u_min,
	u_min,
	u_min,
	u_min,
	u_min,
	u_min,
};
static float ctrl_xu_f32[CTRL_N_UB_CONST] =
{
	u_max,
	u_max,
	u_max,
	u_max,
	u_max,
	u_max,
	u_max,
	u_max,
	u_max,
	u_max,
};
static float ctrl_lm_f32[CTRL_N_EQ_CONST+CTRL_N_INEQ_CONST+CTRL_N_LB_CONST+CTRL_N_UB_CONST] =
{
};

/* Define control matrix variables */
// rows, columns, data array
arm_matrix_instance_f32 ctrl_fBar = {CTRL_N_HORIZON, CTRL_N_STATE, (float32_t *)ctrl_fBar_f32};
arm_matrix_instance_f32 ctrl_f = {CTRL_N_HORIZON, 1, (float32_t *)ctrl_f_f32};
arm_matrix_instance_f32 ctrl_xHat = {CTRL_N_STATE, 1, (float32_t *)ctrl_xHat_f32};
arm_matrix_instance_f32 ctrl_u = {CTRL_N_INPUT, 1, (float32_t *)ctrl_u_f32};

/* Control functions */
void ctrl_init(void)
{
	arm_mat_init_f32(&ctrl_fBar, CTRL_N_HORIZON, CTRL_N_STATE, (float32_t *)ctrl_fBar_f32);
	arm_mat_init_f32(&ctrl_f, CTRL_N_HORIZON, 1, (float32_t *)ctrl_f_f32);
	arm_mat_init_f32(&ctrl_xHat, CTRL_N_STATE, 1, (float32_t *)ctrl_xHat_f32);
	arm_mat_init_f32(&ctrl_u, CTRL_N_INPUT, 1, (float32_t *)ctrl_u_f32);
}

/* Update state vector elements */
void ctrl_set_x1h(float x1)
{
	// Update state x1
	ctrl_xHat_f32[0] = x1;
}
void ctrl_set_x2h(float x2)
{
	// Update state x2
	ctrl_xHat_f32[1] = x2;
}

void ctrl_set_x3h(float x3)
{
	// Update state x3
	ctrl_xHat_f32[2] = x3;
}

void ctrl_set_x4h(float x4)
{
	// Update state x4
	ctrl_xHat_f32[3] = x4;
}

// /* Update control output */
// void ctrl_update(void)
// {
	// // Compute control action
	// arm_mat_mult_f32(&ctrl_mK, &ctrl_x, &ctrl_u);
	// // Update integrator state
	// arm_mat_mult_f32(&ctrl_Az, &ctrl_x, &ctrl_z);
	// // Copy updated value of integrator state into state vector
	// ctrl_x_f32[4] = ctrl_z_f32[0];
// }

void ctrl_update(void)
{
	// Compute f vector
	arm_mat_mult_f32(&ctrl_fBar, &ctrl_xHat, &ctrl_f);

	// Update b vector
	ctrl_b_f32[0] = delta_u_max + ctrl_u_f32[0];
	ctrl_b_f32[CTRL_N_HORIZON] = - ctrl_u_f32[0] - delta_u_min ;

	// Solve for optimal inputs over control horizon
	qpas_sub_noblas(CTRL_N_HORIZON, CTRL_N_EQ_CONST, CTRL_N_INEQ_CONST, CTRL_N_LB_CONST, CTRL_N_UB_CONST, ctrl_H_f32, ctrl_f_f32, ctrl_A_f32, ctrl_b_f32, ctrl_xl_f32, ctrl_xu_f32, ctrl_Ustar_f32, ctrl_lm_f32, 0, &numits, &numadd, &numdrop);

	// Extract first control term
	ctrl_u_f32[0] = ctrl_Ustar_f32[0];

	/* Print functions for debugging. Uncomment to use */
	// printmatrix (CTRL_N_HORIZON,CTRL_N_HORIZON,ctrl_H_f32,CTRL_N_HORIZON,"H");
	// printvector (CTRL_N_HORIZON, ctrl_f_f32, "f");
}


/* Get the current control output */
float getControl(void)
{
	//arm_mat_mult_f32(&ctrl_mK, &ctrl_x, ctrl_u);
	return ctrl_u_f32[0];
}