#ifndef DATA_LOGGING_H
#define DATA_LOGGING_H

/* Add function prototypes here */
void logging_init(void);
void pend_logging_start(void);
void logging_stop(void);
void imu_logging_start(void);

#endif