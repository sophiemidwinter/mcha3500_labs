#include "dummy_task.h"

#include <stdint.h>

#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include "uart.h"
#include "pendulum.h"
#include "motor.h"
#include "IMU.h"
#include "current_sense.h"
#include "kalman_filter.h"
#include "controller.h"

// Define motor parameters
#define N 18.75
// Motor 1
#define Kw1 0.0104
#define Ra1 2.6343
// Motor 2
#define Kw2 0.0106
#define Ra2 2.3547

static void dummy_task_update(void *arg);

static osThreadId_t _dummyTaskThreadID;
static osThreadAttr_t _dummyTaskThreadAttr = 
{
    .name = "heartbeat",
    .priority = osPriorityIdle,
    .stack_size = 128
};

static uint8_t _is_running = 0;
static uint8_t _is_init = 0;

void dummy_task_init(void)
{
    if (!_is_init)
    {
        // CMSIS-RTOS API v2 Timer Documentation: https://www.keil.com/pack/doc/CMSIS/RTOS2/html/group__CMSIS__RTOS__TimerMgmt.html
        _dummyTaskThreadID = osThreadNew(dummy_task_update, NULL, &_dummyTaskThreadAttr);   // Create the thread in the OS scheduler. 
        // Note: The thread starts automatically when osThreadNew is called
        _is_running = 1;
        _is_init = 1;
    }
}

void dummy_task_start(void)
{
    if (!_is_running)
    {
        osThreadResume(_dummyTaskThreadID);
        _is_running = 1;
    }
}

void dummy_task_stop(void)
{
    if (_is_running)
    {
        osThreadSuspend(_dummyTaskThreadID);
        _is_running = 0;
    }
}

uint8_t dummy_task_is_running(void)
{
    return _is_running;
}

void dummy_task_update(void *arg)
{
    UNUSED(arg);
    while(1)
    {	
		
        // Non-blocking delay to wait
        osDelay(100);
		
		// printf("Encoder 1: %ld\n", motor_encoder_1_getValue());
		// printf("Encoder 2: %ld\n", motor_encoder_2_getValue());
		// get_currents();
		// printf("Current 1: %f\n", current_1_getValue());
		// printf("Current 2: %f\n", current_2_getValue());
		// motor1_set(3);
		// motor2_set(3);
		// float omega = kf_get_omega();
		// float angle = kf_get_angle();
		// float current1 = kf_get_current_1();
		// float current2 = kf_get_current_2();
		// printf("%f\n", kf_get_angle());
		// printf("%f\n", getControl());
		//float omega_test = getControl();
		

		
		// Calculate voltages to drive motors
		// float Vin1_test = Kw1*N*omega_test + Ra1*kf_get_current_1();  //0.1(velocity1 - omega);
		// float Vin2_test = Kw2*N*omega_test + Ra2*kf_get_current_2();
		
		// printf("Motor1 %f, %f\n", Vin1_test, omega_test);
		// printf("Motor2 %f, %f\n", Vin2_test, omega_test);
		
		// printf("Velocity 1 %f \n", motor1_get_velocity());
		// printf("Velocity 2 %f \n", motor2_get_velocity());
		
		// Set motor voltages and directions
		//motor1_set(2,1);
		//motor2_set(2,1);
    }
}

void dummy_task_deinit(void)
{
    _is_init = 0;
    _is_running = 0;
}
