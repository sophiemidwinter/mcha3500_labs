#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include <math.h> 
#include "motor.h"
#include "current_sense.h"
#include "kalman_filter.h"
#include "controller.h"
#include "state_machine.h"
#include "motor_control.h"

// Define motor parameters
#define N 18.75
// Motor 1
#define Kw1 0.0104
#define Ra1 2.6343
// Motor 2
#define Kw2 0.0106
#define Ra2 2.3547

static float int1 = 0;
static float int2 = 0;
static float omega;

static float velocity1 = 0;
static float velocity2 = 0;

// Define timer for motor control
static osTimerId_t motorControlTimer_id;
static osTimerAttr_t motorControlTimer_attr =
{
	.name = "motorControlTimer"
};

// Define timer to update actual wheel velocity
static osTimerId_t omegaTimer_id;
static osTimerAttr_t omegaTimer_attr =
{
	.name = "omegaTimer"
};

static uint8_t _is_running = 0;
static uint8_t _is_init = 0;

// Initialise timers
void motor_controller_init(void)
{
    if (!_is_init)
    {   
        motorControlTimer_id = osTimerNew(update_MotorControl, osTimerPeriodic, NULL, &motorControlTimer_attr);  
        if (!osTimerIsRunning(motorControlTimer_id))
        {
            osTimerStart(motorControlTimer_id, 1U);
        }
		omegaTimer_id = osTimerNew(update_omega, osTimerPeriodic, NULL, &omegaTimer_attr);  

        if (!osTimerIsRunning(omegaTimer_id))
        {
            osTimerStart(omegaTimer_id, 5U);
        }
        _is_running = 1;
        _is_init = 1;
    }
}

// Set omega from controller
void ctrl_set_omega(float w)
{
	omega = w;
}

void update_MotorControl(void)
{

	// Use currents from current sensores (not KF as was behaving abnormally)
	get_currents();
	// Calculate voltages to drive motors
	int1 += 0.009*(omega - velocity1);
	int2 += 0.009*(omega - velocity2);
	float Vin1 = Kw1*N*omega + Ra1*current_1_getValue()*0.01 + 0.8*(omega - velocity1) + int1;
	float Vin2 = Kw2*N*omega + Ra2*current_2_getValue()*0.01 + 0.8*(omega - velocity2) + int2;
	
	// Drive motors with control value
	motor1_set(Vin1);
	motor2_set(Vin2);
	
	//printf("%f, %f\n",Vin1, omega);
}

// Update actual motor velocity
void update_omega(void)
{
	velocity1 = motor1_get_velocity();
	velocity2 = motor2_get_velocity();
}

