#include <stdint.h>
#include <stdlib.h>
#include <math.h> 
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h" 
#include "uart.h"
#include "tm_stm32_mpu6050.h"
#include "IMU.h"

/* Variable declarations */
TM_MPU6050_t IMU_datastruct;

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

/* Function definitions */

void IMU_init(void)
{
	/* Initialise IMU with AD0 LOW, accelleration sensitivity +-4g, gyroscope +-250 deg/s */
	TM_MPU6050_Init(&IMU_datastruct, TM_MPU6050_Device_0, TM_MPU6050_Accelerometer_4G, TM_MPU6050_Gyroscope_250s);
}

void IMU_read(void)
{
	/* Read all IMU values */
	TM_MPU6050_ReadAll(&IMU_datastruct);
}

float get_accX(void)
{
	/* Convert accelleration reading to ms^-2 */
	float acceleration_X;
	acceleration_X = (IMU_datastruct.Accelerometer_X/8191.75)*9.81;
	/* return the X acceleration */
	return(acceleration_X);
}

float get_accZ(void)
{
	/* return the Z acceleration */
	float acceleration_Z;
	acceleration_Z = (IMU_datastruct.Accelerometer_Z/8191.75)*9.81;
	return(acceleration_Z);
}

float get_gyoY(void)
{
	/* return the Y angular velocity */
	float angularVelocity_Y;
	angularVelocity_Y = (IMU_datastruct.Gyroscope_Y/131.068)*(M_PI/180.0);
	return(angularVelocity_Y);
}

double get_acc_angle(void)
{
	/* compute IMU angle using accY and accZ using atan2 */
	float acceleration_X;
	float acceleration_Z;
	double theta_acc;
	acceleration_X = get_accX();
	acceleration_Z = get_accZ();
	theta_acc = -atan2(acceleration_X, acceleration_Z);
	
	/* return the IMU angle */
	return(theta_acc);
}
