#ifndef CURRENT_SENSE_H
#define CURRENT_SENSE_H

/* Add function prototypes here */

void current_init(void);
void get_currents(void);
float current_1_getValue(void);
float current_2_getValue(void);

#endif