#ifndef MOTOR_CONTROL_H
#define MOTOR_CONTROL_H

void motor_controller_init(void);
void ctrl_set_omega(float w);
void update_MotorControl(void);
void update_omega(void);

#endif