#include <stdint.h>
#include <stdlib.h>
#include <math.h> 
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h" 
#include "uart.h"
#include "current_sense.h"

uint16_t current_1 = 0, current_2 = 0;

static ADC_HandleTypeDef hadc1;
static ADC_ChannelConfTypeDef sConfigADC;

/* configure ADC for current sensors */
void current_init(void)
{		
		// Enable ADC1 clock 
		__HAL_RCC_ADC1_CLK_ENABLE();
			
		// Enable GPIOA clock
		__HAL_RCC_GPIOA_CLK_ENABLE();

		//Initialise PA6 and PA7
		GPIO_InitTypeDef GPIO_InitStruct;
		GPIO_InitStruct.Pin = GPIO_PIN_6|GPIO_PIN_7;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
		
		// Configure ADC1 instance
		hadc1.Instance = ADC1;
        hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
        hadc1.Init.Resolution = ADC_RESOLUTION_12B;
		hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
		hadc1.Init.ScanConvMode = ENABLE;
		hadc1.Init.ContinuousConvMode = DISABLE;
		hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
		hadc1.Init.NbrOfDiscConversion = 2;
		
		// Configure ADC1 for rank 1 
		sConfigADC.Channel = ADC_CHANNEL_6;
		sConfigADC.Rank = 1;
		sConfigADC.SamplingTime = ADC_SAMPLETIME_480CYCLES;
		sConfigADC.Offset = 0;
		
		// Initialise the ADC1
		HAL_ADC_Init(&hadc1);
		
		// Configure the ADC channel 6
		HAL_ADC_ConfigChannel(&hadc1,&sConfigADC);
		
		// Configure the ADC channel 7
        sConfigADC.Channel = ADC_CHANNEL_7;
		sConfigADC.Rank = 2;
		HAL_ADC_ConfigChannel(&hadc1,&sConfigADC);
}

// Update currents
void get_currents(void)
{
	// Start ADC
	HAL_ADC_Start(&hadc1);
	
	/* Current sensor 1 */
	HAL_ADC_PollForConversion(&hadc1, 100);	// Poll and measure current on the first current sensor (rank 1)
	current_1 = HAL_ADC_GetValue(&hadc1);	// Get return value from ADC conversion
	
	/* Current sensor 2 */
	HAL_ADC_PollForConversion(&hadc1, 100);	// Poll and measure current on the second current sensor (rank 2)
	current_2 = HAL_ADC_GetValue(&hadc1);	// Get return value from ADC conversion 

	// Stop the ADC
	HAL_ADC_Stop(&hadc1);
}

// Return current reading from sensor 1
float current_1_getValue(void)
{
	float voltage1 = current_1*3.3/4095.0;
	voltage1 = voltage1 - 2.5;
	return voltage1/0.4;
}

// Return current reading from sensor 2
float current_2_getValue(void)
{
	float voltage2 = current_2*3.3/4095.0;
	voltage2 = voltage2 - 2.5;
	return - voltage2/0.4;
}


