#include <stdint.h>
#include <stdlib.h>
#include <math.h> 
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h" 
#include "uart.h"
#include "motor.h"

static TIM_HandleTypeDef   _htim2;
static TIM_OC_InitTypeDef  _sConfigPWM;

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

static uint32_t enc_count_1 = 0;
static uint32_t enc_count_2 = 0;

/* Direction pins GPIO init */
/* encoder init x2 */

void motor_init(void)
{
	/* Enable TIM2 clock */
	__TIM2_CLK_ENABLE();
	
	/* Enable GPIOA clock */
	__HAL_RCC_GPIOA_CLK_ENABLE();

	/* Initialise PA0 - Motor 1*/
	GPIO_InitTypeDef GPIO_InitStruct_1;
	GPIO_InitStruct_1.Pin = GPIO_PIN_0;
	GPIO_InitStruct_1.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct_1.Pull = GPIO_NOPULL;
	GPIO_InitStruct_1.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct_1.Alternate = GPIO_AF1_TIM2;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct_1);
	
	/* Initialise PA1 - Motor 2*/
	GPIO_InitTypeDef GPIO_InitStruct_2;
	GPIO_InitStruct_2.Pin = GPIO_PIN_1;
	GPIO_InitStruct_2.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStruct_2.Pull = GPIO_NOPULL;
	GPIO_InitStruct_2.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct_2.Alternate = GPIO_AF1_TIM2;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct_2);

	/* Initialise timer 2 */
	_htim2.Instance = TIM2;
    _htim2.Init.Prescaler = 1;
    _htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
    _htim2.Init.Period = 5000;
    _htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	HAL_TIM_PWM_Init(&_htim2);

	/* Configure timer 2, channel 1 - Motor 1*/
	_sConfigPWM.OCMode = TIM_OCMODE_PWM2;
    _sConfigPWM.Pulse = 0;
    _sConfigPWM.OCPolarity = TIM_OCPOLARITY_LOW;
    _sConfigPWM.OCFastMode = TIM_OCFAST_DISABLE;
	HAL_TIM_PWM_ConfigChannel(&_htim2, &_sConfigPWM, TIM_CHANNEL_1);
	
	/* Configure timer 2, channel 2 - Motor 2*/
	_sConfigPWM.OCMode = TIM_OCMODE_PWM2;
    _sConfigPWM.Pulse = 0;
    _sConfigPWM.OCPolarity = TIM_OCPOLARITY_LOW;
    _sConfigPWM.OCFastMode = TIM_OCFAST_DISABLE;
	HAL_TIM_PWM_ConfigChannel(&_htim2, &_sConfigPWM, TIM_CHANNEL_2);

	/* Set initial Timer 2, channel 1 compare value */
	//__HAL_TIM_SET_COMPARE(&_htim2, TIM_CHANNEL_1, 0);

	/* Start Timer 2, channel 1 - Motor 1*/
    HAL_TIM_PWM_Start(&_htim2, TIM_CHANNEL_1);
	
	/* Set initial Timer 2, channel 2 compare value */
	//__HAL_TIM_SET_COMPARE(&_htim2, TIM_CHANNEL_2, 1000);

	/* Start Timer 2, channel 2 - Motor 2*/
    HAL_TIM_PWM_Start(&_htim2, TIM_CHANNEL_2);	
	
	/* Initialise direction (PB5) - Motor 1 */
	__HAL_RCC_GPIOB_CLK_ENABLE();
	GPIO_InitTypeDef GPIO_InitStruct_3;
	GPIO_InitStruct_3.Pin = GPIO_PIN_5;
	GPIO_InitStruct_3.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct_3.Pull = GPIO_NOPULL;
	GPIO_InitStruct_3.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct_3);
	
	/* Set M1 direction */
	//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);
	
	/* Initialise direction (PB0) - Motor 1 */
	GPIO_InitTypeDef GPIO_InitStruct_4;
	GPIO_InitStruct_4.Pin = GPIO_PIN_0;
	GPIO_InitStruct_4.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct_4.Pull = GPIO_NOPULL;
	GPIO_InitStruct_4.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct_4);
	
	/* Set M2 direction */
	//HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);
	
	/* Initialise D2 (PB10) */
	GPIO_InitTypeDef GPIO_InitStruct_5;
	GPIO_InitStruct_5.Pin = GPIO_PIN_10;
	GPIO_InitStruct_5.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct_5.Pull = GPIO_NOPULL;
	GPIO_InitStruct_5.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct_5);
	
	/* Set D2 high to enable motor driver outputs */
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_SET);
	
	/* Initialise SF (PB6) */
	GPIO_InitTypeDef GPIO_InitStruct_6;
	GPIO_InitStruct_6.Pin = GPIO_PIN_6;
	GPIO_InitStruct_6.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct_6.Pull = GPIO_NOPULL;
	GPIO_InitStruct_6.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct_6);
}

/* Set motor 1 with given voltage and direction */
void motor1_set(float voltage)
{
	// Calculate compare value to set PWM
	float comp_val = (fabsf(voltage)/12.0)*5000;
	__HAL_TIM_SET_COMPARE(&_htim2, TIM_CHANNEL_1, comp_val);
	// Set direcion
	if (voltage >= 0) {
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_RESET);
	}
	else if (voltage < 0) {
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, GPIO_PIN_SET);
	}
}

/* Set motor 2 with given voltage and direction */
void motor2_set(float voltage)
{
	// Calculate compare value to set PWM
	float comp_val = (fabsf(voltage)/12.0)*5000;
	__HAL_TIM_SET_COMPARE(&_htim2, TIM_CHANNEL_2, comp_val);
	// Set direcion
	if (voltage >= 0) {
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);
	}
	else if (voltage < 0) {
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_RESET);
	}
	
}

void motor_encoder_init(void)
{
	/* Enable GPIOC clock */
	__HAL_RCC_GPIOC_CLK_ENABLE();

	/* Initialise PC0, PC1, PC2, PC3 */
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	/* Set priority of external interrupt lines 0,1 to 0x0f, 0x0f
	/* Enable external interrupt for lines 0, 1 */
	/* To find the IRQn_Type definition see "MCHA3500 Windows Toolchain\workspace\STM32Cube_F4_FW\Drivers\
	CMSIS\Device\ST\STM32F4xx\Include\stm32f446xx.h" */
	HAL_NVIC_SetPriority(6, 0x0f, 0);
	HAL_NVIC_EnableIRQ(6);
	HAL_NVIC_SetPriority(7, 0x0f, 1);
	HAL_NVIC_EnableIRQ(7);
	HAL_NVIC_SetPriority(8, 0x0f, 0);
	HAL_NVIC_EnableIRQ(8);
	HAL_NVIC_SetPriority(9, 0x0f, 1);
	HAL_NVIC_EnableIRQ(9);
	
}

void EXTI0_IRQHandler(void)
{
	/* Check if PC0 == PC1. Adjust encoder count accordingly. */
	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))
	{
		enc_count_1 = enc_count_1 + 1;
	}
	else 
	{
		enc_count_1 = enc_count_1 - 1;
	}
	
	/* Reset interrupt */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
}

void EXTI1_IRQHandler(void)
{
	/* Check if PC0 == PC1. Adjust encoder count accordingly. */
	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_0) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_1))
	{
		enc_count_1 = enc_count_1 - 1;
	}
	else 
	{
		enc_count_1 = enc_count_1 + 1;
	}

	/* Reset interrupt */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
}

int32_t motor_encoder_1_getValue(void)
{
	return enc_count_1;
}

void EXTI2_IRQHandler(void)
{
	/* Check if PC2 == PC3. Adjust encoder count accordingly. */
	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_3))
	{
		enc_count_2 = enc_count_2 + 1;
	}
	else 
	{
		enc_count_2 = enc_count_2 - 1;
	}
	
	/* Reset interrupt */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
}

void EXTI3_IRQHandler(void)
{
	/* Check if PC2 == PC3. Adjust encoder count accordingly. */
	if(HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_2) == HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_3))
	{
		enc_count_2 = enc_count_2 - 1;
	}
	else 
	{
		enc_count_2 = enc_count_2 + 1;
	}

	/* Reset interrupt */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_3);
}

int32_t motor_encoder_2_getValue(void)
{
	return enc_count_2;
}

int32_t motor_encoder1_reset(void)
{
	int32_t enc1_counts = motor_encoder_1_getValue();
	enc_count_1 = 0;
	return enc1_counts;
}

int32_t motor_encoder2_reset(void)
{
	int32_t enc2_counts = motor_encoder_2_getValue();
	enc_count_2 = 0;
	return enc2_counts;
}

float motor1_get_velocity(void)
{
	int32_t enc1 = motor_encoder1_reset();
	//float velocity1 = enc1/(64*0.005*18.75)/M_PI;
	float velocity1 = -2*M_PI*enc1/(64*0.005*18.75);
	return velocity1;
}

float motor2_get_velocity(void)
{
	int32_t enc2 = motor_encoder2_reset();
	float velocity2 = 2*M_PI*enc2/(64*0.005*18.75);
	return velocity2;
}


