#ifndef MOTOR_H
#define MOTOR_H

/* Add function prototypes here */

void motor_init(void);
void motor1_set(float voltage);
void motor2_set(float voltage);
void motor_encoder_init(void);
void EXTI0_IRQHandler(void);
void EXTI1_IRQHandler(void);
void EXTI2_IRQHandler(void);
void EXTI3_IRQHandler(void);
int32_t motor_encoder_1_getValue(void);
int32_t motor_encoder_2_getValue(void);
int32_t motor_encoder1_reset(void);
int32_t motor_encoder2_reset(void);
float motor1_get_velocity(void);
float motor2_get_velocity(void);

#endif