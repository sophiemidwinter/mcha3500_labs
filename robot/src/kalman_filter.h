#ifndef KALMAN_FILTER_H
#define KALMAN_FILTER_H

//void kf_timer_init(void);
void kf_timer_init(void);
void kf_init(void);
void run_kf(void);
void kf_set_angle(float x1);
void kf_set_omega(float x2);
void kf_set_current1(float x3);
void kf_set_current2(float x4);
float kf_get_omega(void);
float kf_get_angle(void);
float kf_get_current_1(void);
float kf_get_current_2(void);
enum {
KF_N_INPUT = 1, // number of inputs
KF_N_STATE = 5, // number of states - [omega, theta, bias of accelerometer, current1, current2]
KF_N_OUTPUT = 4, // number of outputs
};

#endif