#ifndef MOD_STATE_MACHINE_H
#define MOD_STATE_MACHINE_H

#include <stdint.h>

typedef enum{
	SM_WAITING,
	SM_RUNNING,
	SM_FAULT,
} statemachineStates;

typedef struct{
	statemachineStates	smState;
} modStateMachineTypeDef;

void module_state_machine_init(void);
void module_state_machine_update(void);
void module_state_machine_start(void);
void module_state_machine_stop(void);

#endif