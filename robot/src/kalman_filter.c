#include <stddef.h>
#include "stm32f4xx_hal.h"
#include "arm_math.h" /* Include STM32 DSP matrix libraries */
#include "cmsis_os2.h"
#include "kalman_filter.h"
#include "IMU.h"
#include "current_sense.h"

#define T_s 0.005 // 200 Hz

// Define Timer
static osTimerId_t kalmanFilterTimer_id;
static osTimerAttr_t kalmanFilterTimer_attr =
{
	.name = "kalmanFilterTimer"
};

static uint8_t _is_running = 0;
static uint8_t _is_init = 0;

void kf_timer_init(void)
{
    if (!_is_init)
    {   
        kalmanFilterTimer_id = osTimerNew(run_kf, osTimerPeriodic, NULL, &kalmanFilterTimer_attr);  
        kf_init();
        if (!osTimerIsRunning(kalmanFilterTimer_id))
        {
            osTimerStart(kalmanFilterTimer_id, 5U);
        }
        _is_running = 1;
        _is_init = 1;
    }
}

/* Define kalman filter arrays */

// State Covariance (from MATLAB run_KF)
static float32_t kf_Q_f32[KF_N_STATE * KF_N_STATE] =
{
	0.002829184323199,		2.081893780532425e-04,		-6.155857937215141e-04, 		0, 		0,
    2.081893780532425e-04,	1.562981737914161e-05,		-3.915735592149394e-05, 		0, 		0,
    -6.155857937215141e-04,	-3.915735592149394e-05,		2.621399261379664e-04,  		0, 		0,
    0,	                	0,	            			0,  						    1e-4, 	0,
    0,	                	0,	            			0,  						    0, 	    1e-4,
};

// Measurement covariance (from MATLAB IMU_covariance)
static float32_t kf_R_f32[KF_N_OUTPUT * KF_N_OUTPUT] =
{
	0.063852402565975,	0,    					0, 		0,
    0,					3.309013665116560e-06,  0, 		0,
    0,                  0, 						0.001, 	0,
    0,                  0, 						0, 		0.001,
};

// A_d matrix of Kalman filter
static float32_t kf_Ad_f32[KF_N_STATE * KF_N_STATE] =
{
	1  , 0, 0, 0, 0,
    T_s, 1, 0, 0, 0,
    0  , 0, 1, 0, 0,
    0  , 0, 0, 1, 0,
    0  , 0, 0, 0, 1,
};

// C matrix of Kalman filter
static float32_t kf_C_f32[KF_N_OUTPUT * KF_N_STATE] =
{
	0, 1, 0, 0, 0,
    1, 0, 1, 0, 0,
    0, 0, 0, 1, 0,
    0, 0, 0, 0, 1,
};

// Initial state estimate (zeros)
static float32_t kf_xm_f32[KF_N_STATE] = 
{
	0,
	0,
	0,
	0,
	0,
};

//Initial estimate covariance (identity)
static float32_t kf_Pm_f32[KF_N_STATE * KF_N_STATE] =
{
	1, 0, 0, 0, 0,
	0, 1, 0, 0, 0,
	0, 0, 1, 0, 0,
	0, 0, 0, 1, 0,
	0, 0, 0, 0, 1,
};

// Initialise space for corrected state estimate
static float32_t kf_xp_f32[KF_N_STATE] =
{
	0,
	0,
	0,
	0,
	0,
};

//Initialise space for corrected measurement error covariance
static float32_t kf_Pp_f32[KF_N_STATE * KF_N_STATE] =
{
	1, 0, 0, 0, 0,
	0, 1, 0, 0, 0,
	0, 0, 1, 0, 0,
	0, 0, 0, 1, 0,
	0, 0, 0, 0, 1,
};

// Measurement array
static float32_t kf_y_f32[KF_N_OUTPUT] =
{
	0,
	0,
	0,
	0,
};

// Gain of Kalman filter
static float32_t kf_Kk_f32[KF_N_STATE*KF_N_OUTPUT] =
{
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0,
};

// Temporary 4x5
static float32_t temp_4x5_f32[KF_N_OUTPUT*KF_N_STATE] = 
{
	0, 0, 0, 0, 0,
	0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0,
};

// Temporary 5x4
static float32_t temp_5x4_f32[KF_N_STATE*KF_N_OUTPUT] = 
{
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0, 
	0, 0, 0, 0,
	0, 0, 0, 0,
};

// Temporary 5x4 #2
static float32_t temp_5x4_2_f32[KF_N_STATE*KF_N_OUTPUT] = 
{
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0, 
	0, 0, 0, 0,
	0, 0, 0, 0,
};

// Temporary 4x4
static float32_t temp_4x4_f32[KF_N_OUTPUT*KF_N_OUTPUT] = 
{
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0, 
	0, 0, 0, 0,
};

// Temporary 4x4 #2
static float32_t temp_4x4_2_f32[KF_N_OUTPUT*KF_N_OUTPUT] = 
{
	0, 0, 0, 0,
	0, 0, 0, 0,
	0, 0, 0, 0, 
	0, 0, 0, 0,
};

// Temporary 4x1
static float32_t temp_4x1_f32[KF_N_OUTPUT] = 
{
	0,
	0,
	0, 
	0,
};

// Temporary 4x1 #2
static float32_t temp_4x1_2_f32[KF_N_OUTPUT] = 
{
	0,
	0,
	0, 
	0,
};

// Temporary 5x1
static float32_t temp_5x1_f32[KF_N_STATE] = 
{
	0,
	0,
	0, 
	0,
	0,
};

// Temporary 5x5
static float32_t temp_5x5_f32[KF_N_STATE*KF_N_STATE] = 
{
	0, 0, 0, 0, 0,
	0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0,
	0, 0, 0, 0, 0,
};

// 5x5 identity matrix
static float32_t eye_5x5_f32[KF_N_STATE*KF_N_STATE] = 
{
	1, 0, 0, 0, 0,
	0, 1, 0, 0, 0,
	0, 0, 1, 0, 0, 
	0, 0, 0, 1, 0,
	0, 0, 0, 0, 1,
};

// Temporary 5x5 #2
static float32_t temp_5x5_2_f32[KF_N_STATE*KF_N_STATE] = 
{
	0, 0, 0, 0, 0,
	0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0,
	0, 0, 0, 0, 0,
};

// Temporary 5x5 #3
static float32_t temp_5x5_3_f32[KF_N_STATE*KF_N_STATE] = 
{
	0, 0, 0, 0, 0,
	0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0,
	0, 0, 0, 0, 0,
};

// Temporary 5x5 #4
static float32_t temp_5x5_4_f32[KF_N_STATE*KF_N_STATE] = 
{
	0, 0, 0, 0, 0,
	0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 
	0, 0, 0, 0, 0,
	0, 0, 0, 0, 0,
};

/* Define control matrix variables */
// rows, columns, data array
arm_matrix_instance_f32 kf_Q = {KF_N_STATE, KF_N_STATE, (float32_t *)kf_Q_f32};
arm_matrix_instance_f32 kf_R = {KF_N_OUTPUT, KF_N_OUTPUT, (float32_t *)kf_R_f32};
arm_matrix_instance_f32 kf_Ad = {KF_N_STATE, KF_N_STATE, (float32_t *)kf_Ad_f32};
arm_matrix_instance_f32 kf_C = {KF_N_OUTPUT, KF_N_STATE, (float32_t *)kf_C_f32};
arm_matrix_instance_f32 kf_xm = {KF_N_STATE, 1, (float32_t *)kf_xm_f32};
arm_matrix_instance_f32 kf_Pm = {KF_N_STATE, KF_N_STATE, (float32_t *)kf_Pm_f32};
arm_matrix_instance_f32 kf_xp = {KF_N_STATE, 1, (float32_t *)kf_xp_f32};
arm_matrix_instance_f32 kf_Pp = {KF_N_STATE, KF_N_STATE, (float32_t *)kf_Pp_f32};
arm_matrix_instance_f32 kf_y = {KF_N_OUTPUT, 1, (float32_t *)kf_y_f32};
arm_matrix_instance_f32 kf_Kk = {KF_N_STATE, KF_N_OUTPUT, (float32_t *)kf_Kk_f32};

// Temp variables to store equations
arm_matrix_instance_f32 temp_4x5 = {KF_N_OUTPUT, KF_N_STATE, (float32_t *)temp_4x5_f32};
arm_matrix_instance_f32 temp_5x4 = {KF_N_STATE, KF_N_OUTPUT, (float32_t *)temp_5x4_f32};
arm_matrix_instance_f32 temp_5x4_2 = {KF_N_STATE, KF_N_OUTPUT, (float32_t *)temp_5x4_2_f32};
arm_matrix_instance_f32 temp_4x4 = {KF_N_OUTPUT, KF_N_OUTPUT, (float32_t *)temp_4x4_f32};
arm_matrix_instance_f32 temp_4x4_2 = {KF_N_OUTPUT, KF_N_OUTPUT, (float32_t *)temp_4x4_2_f32};
arm_matrix_instance_f32 temp_4x1 = {KF_N_OUTPUT, 1, (float32_t *)temp_4x1_f32};
arm_matrix_instance_f32 temp_4x1_2 = {KF_N_OUTPUT, 1, (float32_t *)temp_4x1_2_f32};
arm_matrix_instance_f32 temp_5x1 = {KF_N_STATE, 1, (float32_t *)temp_5x1_f32};
arm_matrix_instance_f32 temp_5x5 = {KF_N_STATE, KF_N_STATE, (float32_t *)temp_5x5_f32};
arm_matrix_instance_f32 eye_5x5 = {KF_N_STATE, KF_N_STATE, (float32_t *)eye_5x5_f32};
arm_matrix_instance_f32 temp_5x5_2 = {KF_N_STATE, KF_N_STATE, (float32_t *)temp_5x5_2_f32};
arm_matrix_instance_f32 temp_5x5_3 = {KF_N_STATE, KF_N_STATE, (float32_t *)temp_5x5_3_f32};
arm_matrix_instance_f32 temp_5x5_4 = {KF_N_STATE, KF_N_STATE, (float32_t *)temp_5x5_4_f32};

/* Kalman functions */
void kf_init(void)
{	
	// Initialise matrix variables
	arm_mat_init_f32(&kf_Q, KF_N_STATE, KF_N_STATE, (float32_t *)kf_Q_f32);
	arm_mat_init_f32(&kf_R, KF_N_OUTPUT, KF_N_OUTPUT, (float32_t *)kf_R_f32);
	arm_mat_init_f32(&kf_Ad, KF_N_STATE, KF_N_STATE, (float32_t *)kf_Ad_f32);
	arm_mat_init_f32(&kf_C, KF_N_OUTPUT, KF_N_STATE, (float32_t *)kf_C_f32);
	arm_mat_init_f32(&kf_xm, KF_N_STATE, 1, (float32_t *)kf_xm_f32);
	arm_mat_init_f32(&kf_Pm, KF_N_STATE, KF_N_STATE, (float32_t *)kf_Pm_f32);
	arm_mat_init_f32(&kf_xp, KF_N_STATE, 1, (float32_t *)kf_xp_f32);
	arm_mat_init_f32(&kf_Pp, KF_N_STATE, KF_N_STATE, (float32_t *)kf_Pp_f32);
	arm_mat_init_f32(&kf_y, KF_N_OUTPUT, 1, (float32_t *)kf_y_f32);
	arm_mat_init_f32(&kf_Kk, KF_N_STATE, KF_N_OUTPUT, (float32_t *)kf_Kk_f32);

	// Initialise temporary variables
	arm_mat_init_f32(&temp_4x5, KF_N_OUTPUT, KF_N_STATE, (float32_t *)temp_4x5_f32);
	arm_mat_init_f32(&temp_5x4, KF_N_STATE, KF_N_OUTPUT, (float32_t *)temp_5x4_f32);
	arm_mat_init_f32(&temp_5x4_2, KF_N_STATE, KF_N_OUTPUT, (float32_t *)temp_5x4_2_f32);
	arm_mat_init_f32(&temp_4x4, KF_N_OUTPUT, KF_N_OUTPUT, (float32_t *)temp_4x4_f32);
	arm_mat_init_f32(&temp_4x4_2, KF_N_OUTPUT, KF_N_OUTPUT, (float32_t *)temp_4x4_2_f32);
	arm_mat_init_f32(&temp_4x1, KF_N_OUTPUT, 1, (float32_t *)temp_4x1_f32);
	arm_mat_init_f32(&temp_4x1_2, KF_N_OUTPUT, 1, (float32_t *)temp_4x1_2_f32);
	arm_mat_init_f32(&temp_5x1, KF_N_STATE, 1, (float32_t *)temp_5x1_f32);
	arm_mat_init_f32(&temp_5x5, KF_N_STATE, KF_N_STATE, (float32_t *)temp_5x5_f32);
	arm_mat_init_f32(&eye_5x5, KF_N_STATE, KF_N_STATE, (float32_t *)eye_5x5_f32);
	arm_mat_init_f32(&temp_5x5_2, KF_N_STATE, KF_N_STATE, (float32_t *)temp_5x5_2_f32);
	arm_mat_init_f32(&temp_5x5_3, KF_N_STATE, KF_N_STATE, (float32_t *)temp_5x5_3_f32);
	arm_mat_init_f32(&temp_5x5_4, KF_N_STATE, KF_N_STATE, (float32_t *)temp_5x5_4_f32);

}

void run_kf(void)
{
	// Pack measurement vector
	get_currents();
	IMU_read();								// read from IMU
	kf_y_f32[0] = get_acc_angle();			// get accelerometer angle measurement		
	kf_y_f32[1] = get_gyoY();				// get gyroscope angular velocity measurement
	kf_y_f32[2] = current_1_getValue();		// get current 1 measurement
	kf_y_f32[3] = current_2_getValue();		// get current 2 measurement
	//printf("%f\n", kf_y_f32[3]);
	
	// CORRECTION STEPS
	
	// Compute Kalman gain (Kk = Pm*C.'/(C*Pm*C.' + R))
	arm_mat_mult_f32(&kf_C, &kf_Pm, &temp_4x5);				// C*Pm
	arm_mat_trans_f32(&kf_C, &temp_5x4);					// C.'
	arm_mat_mult_f32(&kf_Pm, &temp_5x4, &temp_5x4_2);		// Pm*C.'
	arm_mat_mult_f32(&temp_4x5, &temp_5x4, &temp_4x4);		// C*Pm*C.'
	arm_mat_add_f32(&temp_4x4, &kf_R, &temp_4x4_2);			// C*Pm*C.' + R
	arm_mat_inverse_f32(&temp_4x4_2, &temp_4x4);			// inv(C*Pm*C.' + R)
	arm_mat_mult_f32(&temp_5x4_2, &temp_4x4, &kf_Kk);		// Pm*C.'*inv(C*Pm*C.' + R)
	
	// Corrected state estimate (xp = xm + Kk*(yi - C*xm))
	arm_mat_mult_f32(&kf_C, &kf_xm, &temp_4x1);				// C*xm
	arm_mat_sub_f32(&kf_y, &temp_4x1, &temp_4x1_2);			// yi - C*xm
	arm_mat_mult_f32(&kf_Kk, &temp_4x1_2, &temp_5x1);		// Kk*(yi - C*xm)
	arm_mat_add_f32(&kf_xm, &temp_5x1, &kf_xp);				// xm + Kk*(yi - C*xm)
		
	// Corrected measurement error covariances (Pp = (eye(3) - Kk*C)*Pm*(eye(3) - Kk*C).' + Kk*R*Kk.')
	arm_mat_mult_f32(&kf_Kk, &kf_C, &temp_5x5);				// Kk*C
	arm_mat_sub_f32(&eye_5x5, &temp_5x5, &temp_5x5_2);		// eye(3) - Kk*C
	arm_mat_trans_f32(&temp_5x5_2, &temp_5x5_3);			// (eye(5) - Kk*C).'
	arm_mat_mult_f32(&temp_5x5_2, &kf_Pm, &temp_5x5);		// (eye(5) - Kk*C)*Pm
	arm_mat_mult_f32(&temp_5x5, &temp_5x5_3, &temp_5x5_2);	// (eye(5) - Kk*C)*Pm*(eye(3) - Kk*C).'
	arm_mat_trans_f32(&kf_Kk, &temp_4x5);					// Kk.'
	arm_mat_mult_f32(&kf_Kk, &kf_R, &temp_5x4);				// Kk*R
	arm_mat_mult_f32(&temp_5x4, &temp_4x5, &temp_5x5_3);	// Kk*R*Kk.'
	arm_mat_add_f32(&temp_5x5_2, &temp_5x5_3, &kf_Pp);		// (eye(3) - Kk*C)*Pm*(eye(3) - Kk*C).' + Kk*R*Kk.'
		
	// PREDICTION STEPS	
	
	// Compute predicted new state (xm = Ad*xp)
	arm_mat_mult_f32(&kf_Ad, &kf_xp, &kf_xm);				// Ad*xp
		
	// Compute predicted error covariance (Pm = Ad*Pp*Ad.' + Q)
	arm_mat_trans_f32(&kf_Ad, &temp_5x5);					// Ad.'
	arm_mat_mult_f32(&kf_Ad, &kf_Pp, &temp_5x5_2);			// Ad*Pp
	arm_mat_mult_f32(&temp_5x5_2, &temp_5x5, &temp_5x5_3);	// Ad*Pp*Ad.'
	arm_mat_add_f32(&temp_5x5_3, &kf_Q, &kf_Pm);			// Ad*Pp*Ad.' + Q
}

float kf_get_omega(void)
{
     return kf_xm_f32[0];
}

float kf_get_angle(void)
{
     return kf_xm_f32[1];
}

float kf_get_current_1(void)
{
     return kf_xm_f32[3]; 
}

float kf_get_current_2(void)
{
     return kf_xm_f32[4]; 
}