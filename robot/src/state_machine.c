#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h"
#include <math.h> 
#include "motor.h"
#include "current_sense.h"
#include "IMU.h"
#include "kalman_filter.h"
#include "controller.h"
#include "state_machine.h"
#include "motor_control.h"

// Define variables for controller
#define yref 0.0
#define mw 0.0633
#define mc 1.3381
#define r 0.045
#define l 0.2063
#define Jw 0.0
#define Jc 0.0

// Define motor parameters
#define N 18.75
// Motor 1
#define Kw1 0.0104
#define Ra1 2.6343
// Motor 2
#define Kw2 0.0106
#define Ra2 2.3547

float theta = 0;

void _runStateMachine(void *argument);

static osTimerId_t stateMachineTimer_id;
static osTimerAttr_t stateMachineTimer_attr =
{
	.name = "stateMachineTimer"
};

modStateMachineTypeDef sStateMachineState = 
{
    .smState = SM_FAULT
}; //State machine structure


void module_state_machine_update(void)
{
    // State Machine
    switch(sStateMachineState.smState)
    {
        case SM_WAITING:
		//printf("WAITING\n");
		
		// Read IMU
		IMU_read();
		
		// Read current sensors
		get_currents();
		
		// If angle exceeds +- 45deg set state to fault
				if(fabs(get_acc_angle()) > 0.785)
		{
			sStateMachineState.smState = SM_FAULT;
		}
		// If current to motors exceeds +- 1.5A set state to fault
		else if(fabsf(current_1_getValue()) > 1.5 || fabsf(current_2_getValue()) > 1.5)
		{
			sStateMachineState.smState = SM_FAULT;
		}
		else
		{
			sStateMachineState.smState = SM_RUNNING;
		}
        break;
		
		case SM_RUNNING:
		//printf("RUNNING\n");
		
		// Get theta from Kalman filter
		theta = kf_get_angle() + 0.06;
		
		// Find phi_dot and theta_dot
		float phi_dot = getControl();	// Read velocity (assuming both motors will be similar enough?)
		float theta_dot = kf_get_omega();
		
		// Compute ptheta (taking theta = alpha = 0)
		float ptheta = phi_dot*(0.01526008635) + theta_dot*(0.084631458889000); 
		
		// Set controller states
		ctrl_set_x1h(theta);
		ctrl_set_x2h(ptheta);
		
		// Set yref
		ctrl_set_yref(yref);
		
		// Run controller
		ctrl_update();

		// Send desired velocity to motor controller
		ctrl_set_omega(phi_dot);

		// Check for faults
		if(fabs(get_acc_angle()) > 0.785)
		{
			sStateMachineState.smState = SM_FAULT;
		}
		// If current to motors exceeds +- 1.5A set state to fault
		else if(fabsf(current_1_getValue()) > 1.5 || fabsf(current_2_getValue()) > 1.5)
		{
			sStateMachineState.smState = SM_FAULT;
		}
		
		break;
		
		case SM_FAULT:
		
		//printf("FAULT\n");
		
		// Turn motors off
		motor1_set(0);
		motor2_set(0);
		
		// Reset encoders
		motor_encoder1_reset();
		motor_encoder2_reset();
		
		// Read IMU
		IMU_read();
		
		// Read current sensors
		get_currents();
		
		// If angle exceeds +- 45deg set state to fault
		if(fabs(get_acc_angle()) < 0.785 && fabsf(current_1_getValue()) < 1.5 && fabsf(current_2_getValue()) < 1.5)
		{
			sStateMachineState.smState = SM_WAITING;
		}
		break;

    }
}

void _runStateMachine(void *argument)
{
    UNUSED(argument);
	module_state_machine_update();
}

void module_state_machine_init(void)
{
	printf("INIT\n");
        // Initialise an os timer to tigger the state machine
        stateMachineTimer_id = osTimerNew(_runStateMachine, osTimerPeriodic, NULL, &stateMachineTimer_attr);
		osTimerStart(stateMachineTimer_id, 10U);
		sStateMachineState.smState = SM_WAITING;
}