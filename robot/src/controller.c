#include <stddef.h>
#include "stm32f4xx_hal.h"
#include "arm_math.h" /* Include STM32 DSP matrix libraries */
#include "qpas_sub_noblas.h"
#include "controller.h"

#define T_s 0.01 // 100 Hz

// Variables for QP solver
int numits,numadd,numdrop;

// Define actuator limits
#define u_min -60.0
#define u_max 60.0
#define delta_u_min -0.3
#define delta_u_max 0.3

// Working ish (Q = 10 1 3, R = 1)
// Hessian
static float32_t ctrl_H_f32[CTRL_N_HORIZON*CTRL_N_HORIZON] =
{
0.028860,  0.008315,  0.007800,  0.007312,  0.006850,  0.006414,  0.006001,  0.005611,  0.005242,  0.004892,  
0.008315,  0.027810,  0.007327,  0.006870,  0.006438,  0.006029,  0.005642,  0.005276,  0.004930,  0.004603,  
0.007800,  0.007327,  0.026880,  0.006453,  0.006048,  0.005665,  0.005303,  0.004960,  0.004636,  0.004329,  
0.007312,  0.006870,  0.006453,  0.026058,  0.005680,  0.005321,  0.004982,  0.004661,  0.004358,  0.004071,  
0.006850,  0.006438,  0.006048,  0.005680,  0.025331,  0.004997,  0.004679,  0.004379,  0.004095,  0.003826,  
0.006414,  0.006029,  0.005665,  0.005321,  0.004997,  0.024689,  0.004393,  0.004112,  0.003847,  0.003595,  
0.006001,  0.005642,  0.005303,  0.004982,  0.004679,  0.004393,  0.024121,  0.003860,  0.003612,  0.003377,  
0.005611,  0.005276,  0.004960,  0.004661,  0.004379,  0.004112,  0.003860,  0.023621,  0.003390,  0.003171,  
0.005242,  0.004930,  0.004636,  0.004358,  0.004095,  0.003847,  0.003612,  0.003390,  0.023180,  0.002976,  
0.004892,  0.004603,  0.004329,  0.004071,  0.003826,  0.003595,  0.003377,  0.003171,  0.002976,  0.022791,
};

// f bar
static float32_t ctrl_fBar_f32[CTRL_N_HORIZON*CTRL_N_STATE] =
{
-5.567935,  -11.698219,  -0.093509,  
-5.212715,  -10.952171,  -0.085572,  
-4.877033,  -10.247139,  -0.078104,  
-4.559812,  -9.580863,  -0.071079,  
-4.260033,  -8.951204,  -0.064475,  
-3.976734,  -8.356145,  -0.058267,  
-3.709006,  -7.793776,  -0.052433,  
-3.455988,  -7.262294,  -0.046953,  
-3.216870,  -6.759993,  -0.041808,  
-2.990881,  -6.285261,  -0.036979, 
};

// f
static float32_t ctrl_f_f32[CTRL_N_HORIZON] =
{
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
};
// State vector
static float32_t ctrl_xHat_f32[CTRL_N_STATE] =
{
	0.0,
	0.0,
	0.0,
};
// Control
static float32_t ctrl_u_f32[CTRL_N_INPUT] =
{
	0.0,
};
// U star
static float32_t ctrl_Ustar_f32[CTRL_N_HORIZON*CTRL_N_INPUT] =
{
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
};
// Constraints
static float ctrl_A_f32[CTRL_N_INEQ_CONST*CTRL_N_HORIZON] =
{
     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,
     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,
     0.0,     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,
     0.0,     0.0,     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,     0.0,     0.0,     0.0,     0.0,
     0.0,     0.0,     0.0,     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,     0.0,     0.0,     0.0,
     0.0,     0.0,     0.0,     0.0,     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,     0.0,     0.0,
     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,     0.0,
     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,     0.0,
     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     1.0,    -1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,     1.0,
     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     1.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,     0.0,    -1.0,
};
static float ctrl_b_f32[CTRL_N_INEQ_CONST] =
{
	delta_u_max,
	delta_u_max,
	delta_u_max,
	delta_u_max,
	delta_u_max,
	delta_u_max,
	delta_u_max,
	delta_u_max,
	delta_u_max,
	delta_u_max,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
	-delta_u_min,
};
static float ctrl_xl_f32[CTRL_N_LB_CONST] =
{
	u_min,
	u_min,
	u_min,
	u_min,
	u_min,
	u_min,
	u_min,
	u_min,
	u_min,
	u_min,
};
static float ctrl_xu_f32[CTRL_N_UB_CONST] =
{
	u_max,
	u_max,
	u_max,
	u_max,
	u_max,
	u_max,
	u_max,
	u_max,
	u_max,
	u_max,
};
static float ctrl_lm_f32[CTRL_N_EQ_CONST+CTRL_N_INEQ_CONST+CTRL_N_LB_CONST+CTRL_N_UB_CONST] =
{
};
// Initialise y reference
static float32_t ctrl_yref_f32[CTRL_N_INPUT] =
{
	0.0,
};
// Initialise x_star
static float32_t ctrl_xstar_f32[CTRL_N_STATE] =
{
	0.0,
	0.0,
	0.0,
};
// Initialise u_star
static float32_t ctrl_uustar_f32[CTRL_N_HORIZON] =
{
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	
};
// Nx
static float32_t ctrl_Nx_f32[CTRL_N_STATE] =
{
	0.0,
	0.015205893300000,
	0.0,
};
// Nu
static float32_t ctrl_Nu_f32[CTRL_N_HORIZON] =
{
	1.0,
};
// Temp CTRL_N_HORIZONx1
static float32_t ctrl_tempN_Horx1_f32[CTRL_N_HORIZON] =
{
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
};

// Temp CTRL_N_HORIZONx1 # 2
static float32_t ctrl_tempN_Horx1_2_f32[CTRL_N_HORIZON] =
{
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
};

// Temp CTRL_N_HORIZONx1 # 3
static float32_t ctrl_tempN_Horx1_3_f32[CTRL_N_HORIZON] =
{
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
};

// Temp CTRL_N_HORIZONx1 # 4
static float32_t ctrl_tempN_Horx1_4_f32[CTRL_N_HORIZON] =
{
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
	0.0,
};

//Temp 1x1
static float32_t ctrl_temp1x1_f32[1] =
{
	0.0,
};

/* Define control matrix variables */
// rows, columns, data array
arm_matrix_instance_f32 ctrl_H = {CTRL_N_HORIZON, CTRL_N_HORIZON, (float32_t *)ctrl_H_f32};
arm_matrix_instance_f32 ctrl_fBar = {CTRL_N_HORIZON, CTRL_N_STATE, (float32_t *)ctrl_fBar_f32};
arm_matrix_instance_f32 ctrl_f = {CTRL_N_HORIZON, 1, (float32_t *)ctrl_f_f32};
arm_matrix_instance_f32 ctrl_xHat = {CTRL_N_STATE, 1, (float32_t *)ctrl_xHat_f32};
arm_matrix_instance_f32 ctrl_u = {CTRL_N_INPUT, 1, (float32_t *)ctrl_u_f32};
arm_matrix_instance_f32 ctrl_yref = {CTRL_N_INPUT, 1, (float32_t *)ctrl_yref_f32};
arm_matrix_instance_f32 ctrl_xstar = {CTRL_N_STATE, 1, (float32_t *)ctrl_xstar_f32};
arm_matrix_instance_f32 ctrl_uustar = {CTRL_N_INPUT, 1, (float32_t *)ctrl_uustar_f32};
arm_matrix_instance_f32 ctrl_Nx = {CTRL_N_STATE, 1, (float32_t *)ctrl_Nx_f32};
arm_matrix_instance_f32 ctrl_Nu = {1, 1, (float32_t *)ctrl_Nu_f32};
arm_matrix_instance_f32 ctrl_tempN_Horx1 = {CTRL_N_HORIZON, 1, (float32_t *)ctrl_tempN_Horx1_f32};
arm_matrix_instance_f32 ctrl_tempN_Horx1_2 = {CTRL_N_HORIZON, 1, (float32_t *)ctrl_tempN_Horx1_2_f32};
arm_matrix_instance_f32 ctrl_tempN_Horx1_3 = {CTRL_N_HORIZON, 1, (float32_t *)ctrl_tempN_Horx1_3_f32};
arm_matrix_instance_f32 ctrl_tempN_Horx1_4 = {CTRL_N_HORIZON, 1, (float32_t *)ctrl_tempN_Horx1_4_f32};
arm_matrix_instance_f32 ctrl_temp1x1 = {1, 1, (float32_t *)ctrl_temp1x1_f32};

/* Control functions */
void ctrl_init(void)
{
	arm_mat_init_f32(&ctrl_H, CTRL_N_HORIZON, CTRL_N_HORIZON, (float32_t *)ctrl_H_f32);
	arm_mat_init_f32(&ctrl_fBar, CTRL_N_HORIZON, CTRL_N_STATE, (float32_t *)ctrl_fBar_f32);
	arm_mat_init_f32(&ctrl_f, CTRL_N_HORIZON, 1, (float32_t *)ctrl_f_f32);
	arm_mat_init_f32(&ctrl_xHat, CTRL_N_STATE, 1, (float32_t *)ctrl_xHat_f32);
	arm_mat_init_f32(&ctrl_u, CTRL_N_INPUT, 1, (float32_t *)ctrl_u_f32);
	arm_mat_init_f32(&ctrl_yref, CTRL_N_INPUT, 1, (float32_t *)ctrl_yref_f32);
	arm_mat_init_f32(&ctrl_xstar, CTRL_N_STATE, 1, (float32_t *)ctrl_xstar_f32);
	arm_mat_init_f32(&ctrl_uustar, CTRL_N_INPUT, 1, (float32_t *)ctrl_uustar_f32);
	arm_mat_init_f32(&ctrl_Nx, CTRL_N_STATE, 1, (float32_t *)ctrl_Nx_f32);
	arm_mat_init_f32(&ctrl_Nu, 1, 1, (float32_t *)ctrl_Nu_f32);
	arm_mat_init_f32(&ctrl_tempN_Horx1, CTRL_N_HORIZON, 1, (float32_t *)ctrl_tempN_Horx1_f32);
	arm_mat_init_f32(&ctrl_tempN_Horx1_2, CTRL_N_HORIZON, 1, (float32_t *)ctrl_tempN_Horx1_2_f32);
	arm_mat_init_f32(&ctrl_tempN_Horx1_3, CTRL_N_HORIZON, 1, (float32_t *)ctrl_tempN_Horx1_3_f32);
	arm_mat_init_f32(&ctrl_tempN_Horx1_4, CTRL_N_HORIZON, 1, (float32_t *)ctrl_tempN_Horx1_4_f32);
	arm_mat_init_f32(&ctrl_temp1x1, 1, 1, (float32_t *)ctrl_temp1x1_f32);
}

/* Update state vector elements */
void ctrl_set_x1h(float x1)
{
	// Update state x1
	ctrl_xHat_f32[0] = x1;
}
void ctrl_set_x2h(float x2)
{
	// Update state x2
	ctrl_xHat_f32[1] = x2;
}

/* Set yref */
void ctrl_set_yref(float yref)
{
	ctrl_yref_f32[0] = yref;
}

void ctrl_update(void)
{
	// Compute x_star
	arm_mat_mult_f32(&ctrl_Nx, &ctrl_yref, &ctrl_xstar);
	
	// Compute u_star
	arm_mat_mult_f32(&ctrl_Nu, &ctrl_yref, &ctrl_temp1x1);
	
	ctrl_uustar_f32[0] = ctrl_temp1x1_f32[0];
	ctrl_uustar_f32[1] = ctrl_temp1x1_f32[0];
	ctrl_uustar_f32[2] = ctrl_temp1x1_f32[0];
	ctrl_uustar_f32[3] = ctrl_temp1x1_f32[0];
	ctrl_uustar_f32[4] = ctrl_temp1x1_f32[0];
	ctrl_uustar_f32[5] = ctrl_temp1x1_f32[0];
	ctrl_uustar_f32[6] = ctrl_temp1x1_f32[0];
	ctrl_uustar_f32[7] = ctrl_temp1x1_f32[0];
	ctrl_uustar_f32[8] = ctrl_temp1x1_f32[0];
	ctrl_uustar_f32[9] = ctrl_temp1x1_f32[0];
	
	// Compute f vector
	arm_mat_mult_f32(&ctrl_fBar, &ctrl_xHat, &ctrl_tempN_Horx1); 					// obj.fBar_MPC*[x; obj.z]
	arm_mat_mult_f32(&ctrl_fBar, &ctrl_xstar ,&ctrl_tempN_Horx1_2); 				// obj.fBar_MPC*[x_star;0]
	arm_mat_mult_f32(&ctrl_H, &ctrl_uustar, &ctrl_tempN_Horx1_3); 					// obj.H_MPC*U_Star_MPC
	arm_mat_sub_f32(&ctrl_tempN_Horx1, &ctrl_tempN_Horx1_2, &ctrl_tempN_Horx1_4);   // obj.fBar_MPC*[x; obj.z] - obj.fBar_MPC*[x_star;0]
	arm_mat_sub_f32(&ctrl_tempN_Horx1_4, &ctrl_tempN_Horx1_3, &ctrl_f);			    // f_MPC = obj.fBar_MPC*[x; obj.z] - obj.fBar_MPC*[x_star;0] - obj.H_MPC*U_Star_MPC
	
	// Update b vector
	ctrl_b_f32[0] = delta_u_max + ctrl_u_f32[0];
	ctrl_b_f32[CTRL_N_HORIZON] = - ctrl_u_f32[0] - delta_u_min ;

	// Solve for optimal inputs over control horizon
	qpas_sub_noblas(CTRL_N_HORIZON, CTRL_N_EQ_CONST, CTRL_N_INEQ_CONST, CTRL_N_LB_CONST, CTRL_N_UB_CONST, ctrl_H_f32, ctrl_f_f32, ctrl_A_f32, ctrl_b_f32, ctrl_xl_f32, ctrl_xu_f32, ctrl_Ustar_f32, ctrl_lm_f32, 0, &numits, &numadd, &numdrop);
	
	// Extract first control term
	ctrl_u_f32[0] = ctrl_Ustar_f32[0];
	
	// Update integrator state
	//arm_mat_mult_f32(&ctrl_Az, &ctrl_x, &ctrl_z);
	ctrl_xHat_f32[2] = ctrl_xHat_f32[2] + T_s*(ctrl_u_f32[0] - ctrl_uustar_f32[0]);
	
	/* Print functions for debugging. Uncomment to use */
	// printmatrix (CTRL_N_HORIZON,CTRL_N_HORIZON,ctrl_H_f32,CTRL_N_HORIZON,"H");
	// printvector (CTRL_N_HORIZON, ctrl_f_f32, "f");
}


/* Get the current control output */
float getControl(void)
{
	//arm_mat_mult_f32(&ctrl_mK, &ctrl_x, ctrl_u);
	return ctrl_u_f32[0];
}