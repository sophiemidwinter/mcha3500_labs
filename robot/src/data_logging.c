#include <stdint.h>
#include <stdlib.h>
#include <math.h> 
#include "stm32f4xx_hal.h"
#include "cmsis_os2.h" 
#include "uart.h"
#include "data_logging.h"
#include "pendulum.h"
#include "IMU.h"
#include "motor.h"
#include "current_sense.h"
#include "kalman_filter.h"


#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif

/* Variable declarations */
uint16_t logCount = 0;
float voltage;
float ang_velocity;
float current;
float velocity;
double imu_angle;
float current1;
float current2;
static osTimerId_t dataLoggingTimerId;
static osTimerAttr_t dataLoggingTimerAttr = 
{
	.name = "dataLoggingTimer"
};
static void (*log_function)(void);

/* Function declarations */
static void log_pendulum(void);
static void log_pointer(void *argument);
static void log_imu(void);
static void log_motor1(void);
static void log_motor2(void);
static void log_kalmanTest(void);
static void log_kalmanFilter(void);



/* Function definitions */

void logging_init(void)
{
	/* Initialise timer for use with pendulum data logging */
	dataLoggingTimerId = osTimerNew(log_pointer, osTimerPeriodic, NULL, &dataLoggingTimerAttr);
}

static void log_pointer(void *argument)
{
	UNUSED(argument);
	
	/* Call function pointed to by log_function */
	(*log_function)();
}

void pend_logging_start(void)
{
	log_function = &log_pendulum;
	logCount = 0;
	osTimerStart(dataLoggingTimerId, 5);
}

void logging_stop(void)
{
	osTimerStop(dataLoggingTimerId);
}

static void log_pendulum(void)
{
	/* Supress compiler warnings for unused arguments
	UNUSED(argument);*/

	/* Read the encoder counts */
	float encoder = motor_encoder_1_getValue();
	
	/* Print the sample time and encoder counts to the serial terminal in the format [time],[encoder] */
	printf("%f,%f\n", logCount*0.005, encoder);	

	/* Increment log count */
	logCount ++;

	/* Stop logging once 10 seconds is reached */
	if(logCount*0.005 >= 10)
	{
		logging_stop();
	}
}

void imu_logging_start(void)
{
	log_function = &log_imu;
	logCount = 0;
	osTimerStart(dataLoggingTimerId, 5);
}

static void log_imu(void)
{
	/* Read IMU */
	IMU_read();

	/* Get the imu angle from accelerometer readings */
	imu_angle = get_acc_angle();
	
	/* Get the imu X gyro reading */
	ang_velocity = get_gyoY();
	

	/* Read the encoder counts */
	float encoder = motor_encoder_1_getValue();

	/* Print the time, accelerometer angle, gyro angular velocity and encoder values to the serial terminal in the format %f,%f,%f,%f\n */
	printf("%f,%f,%f,%f\n", logCount*0.005, imu_angle*(180/3.14159265358979323846), ang_velocity, encoder);
	
	/* Increment log count */
	logCount ++;

	/* Stop logging once 10 seconds is reached */
	if(logCount*0.005 >= 10)
	{
		logging_stop();
	}
}

/* Motor logging functions */

// Start logging motor 1
void motor1_logging_start(void)
{
	log_function = &log_motor1;
	logCount = 0;
	osTimerStart(dataLoggingTimerId, 5);
}

// Log motor 1
static void log_motor1(void)
{
	/* Supress compiler warnings for unused arguments
	UNUSED(argument);*/

	// Set motor 1 with sine wave
	float Vsin = 4*sinf(2*M_PI*3*logCount*0.005);
	motor1_set(Vsin);

	// Read from motor 1 current sensor
	get_currents();
	current = current_1_getValue();
	
	// Get angular velocity from IMU
	//ang_velocity = get_gyoY();
	velocity = motor1_get_velocity();
	
	/* Print the sample time, voltage, current, and velocity to the serial terminal in the format [time],[voltage],[current],[velocity] */
	printf("%f,%f,%f,%f\n", logCount*0.005, Vsin, current, velocity);	

	/* Increment log count */
	logCount ++;

	/* Stop logging once 10 seconds is reached */
	if(logCount*0.005 >= 10)
	{
		logging_stop();
	}
}

// Start logging motor 2
void motor2_logging_start(void)
{
	log_function = &log_motor2;
	logCount = 0;
	osTimerStart(dataLoggingTimerId, 5);
}

// Log motor 2
static void log_motor2(void)
{
	/* Supress compiler warnings for unused arguments
	UNUSED(argument);*/

	// Set motor 1 with sine wave
	float Vsin = 2*sinf(2*M_PI*1*logCount*0.005);
	motor2_set(Vsin);

	// Read from motor 1 current sensor
	get_currents();
	current = current_2_getValue();
	
	// Get angular velocity from IMU
	//ang_velocity = get_gyoY();
	velocity = motor2_get_velocity();
	
	/* Print the sample time, voltage, current, and velocity to the serial terminal in the format [time],[voltage],[current],[velocity] */
	printf("%f,%f,%f,%f\n", logCount*0.005, Vsin, current, velocity);	

	/* Increment log count */
	logCount ++;

	/* Stop logging once 10 seconds is reached */
	if(logCount*0.005 >= 10)
	{
		logging_stop();
	}
}

// Start logging KF test
void kalmanTest_logging_start(void)
{
	log_function = &log_kalmanTest;
	logCount = 0;
	osTimerStart(dataLoggingTimerId, 5);
}

static void log_kalmanTest(void)
{
	/* Supress compiler warnings for unused arguments
	UNUSED(argument);*/
	
	float Vsin = 4*sinf(2*M_PI*1*logCount*0.005);
	motor1_set(Vsin);
	motor2_set(Vsin);
	

	/* Read current sensors */
	get_currents();
	current1 = current_1_getValue();
	current2 = current_2_getValue();
	
	/* Get the imu angle from accelerometer readings */
	imu_angle = get_acc_angle();
	
	/* Get the imu X gyro reading */
	ang_velocity = get_gyoY();
	
	/* Print the sample time and encoder counts to the serial terminal */
	printf("%f,%f,%f,%f,%f\n", logCount*0.005, imu_angle, ang_velocity, current1, current2);	

	/* Increment log count */
	logCount ++;

	/* Stop logging once 10 seconds is reached */
	if(logCount*0.005 >= 10)
	{
		logging_stop();
	}
}

// Start logging Kalman Filter
void kalmanFilter_logging_start(void)
{
	log_function = &log_kalmanFilter;
	logCount = 0;
	osTimerStart(dataLoggingTimerId, 5);
}

static void log_kalmanFilter(void)
{
	/* Supress compiler warnings for unused arguments
	UNUSED(argument);*/
	float Vsin = 4*sinf(2*M_PI*1*logCount*0.005);
	motor1_set(3);
	motor2_set(3);
	
	
	IMU_read();
	get_currents();
		
	/* Print the sample time and encoder counts to the serial terminal */
	float kf_c2 = kf_get_current_2();
	printf("%f,%f,%f,%f,%f\n", logCount*0.005, current_1_getValue(), kf_get_current_1(), current_2_getValue(), kf_c2);	

	/* Increment log count */
	logCount ++;

	/* Stop logging once 10 seconds is reached */
	if(logCount*0.005 >= 10)
	{
		logging_stop();
	}
}
